#include <Python.h>
#include "structmember.h"
#include "datetime.h"

#include <time.h>

#include <x52pro.h>

enum x52pro_buttons
{
	X52PRO_BUTTON_TRIGGER1 = 0,
	X52PRO_BUTTON_FIRE,
	X52PRO_BUTTON_A,
	X52PRO_BUTTON_B,
	X52PRO_BUTTON_C,
	X52PRO_BUTTON_PINKIE,
	X52PRO_BUTTON_D,
	X52PRO_BUTTON_E,
	X52PRO_BUTTON_T1,
	X52PRO_BUTTON_T2,
	X52PRO_BUTTON_T3,
	X52PRO_BUTTON_T4,
	X52PRO_BUTTON_T5,
	X52PRO_BUTTON_T6,
	X52PRO_BUTTON_TRIGGER2,
	X52PRO_BUTTON_MOUSE_CLICK,
	X52PRO_BUTTON_MOUSE_SCROLL_FORWARD,
	X52PRO_BUTTON_MOUSE_SCROLL_BACK,
	X52PRO_BUTTON_MOUSE_SCROLL_CLICK,
	X52PRO_BUTTON_COOLIE_UP,
	X52PRO_BUTTON_COOLIE_RIGHT,
	X52PRO_BUTTON_COOLIE_DOWN,
	X52PRO_BUTTON_COOLIE_LEFT,
	X52PRO_BUTTON_THROTTLEHAT_BACK,
	X52PRO_BUTTON_THROTTLEHAT_RIGHT,
	X52PRO_BUTTON_THROTTLEHAT_FORWARD,
	X52PRO_BUTTON_THROTTLEHAT_LEFT,
	X52PRO_BUTTON_MODE1,
	X52PRO_BUTTON_MODE2,
	X52PRO_BUTTON_MODE3,
	X52PRO_BUTTON_I,
	X52PRO_BUTTON_MFD_FUNCTION,
	X52PRO_BUTTON_MFD_STARTSTOP,
	X52PRO_BUTTON_MFD_RESET,
	X52PRO_BUTTON_MFD_PAGE_UP,
	X52PRO_BUTTON_MFD_PAGE_DOWN,
	X52PRO_BUTTON_MFD_SEL_UP,
	X52PRO_BUTTON_MFD_SEL_DOWN,
	X52PRO_BUTTON_MFD_SEL
};

typedef struct
{
	PyObject_HEAD
	struct x52 *handle;
} PyX52_X52_object;

static PyTypeObject PyX52_X52_type;
static PyMethodDef PyX52_methods[];
static PyMethodDef PyX52_X52_methods[];

static PyObject *PyX52_X52_new(PyTypeObject *);
static int PyX52_X52_init(PyX52_X52_object *);
static void PyX52_X52_dealloc(PyX52_X52_object *);

static PyObject *PyX52_X52_gettype(PyX52_X52_object *);
static PyObject *PyX52_X52_settext(PyX52_X52_object *, PyObject *, PyObject *);
static PyObject *PyX52_X52_setbri(PyX52_X52_object *, PyObject *, PyObject *);
static PyObject *PyX52_X52_setled(PyX52_X52_object *, PyObject *, PyObject *);
static PyObject *PyX52_X52_settime(PyX52_X52_object *, PyObject *, PyObject *);
static PyObject *PyX52_X52_setoffs(PyX52_X52_object *, PyObject *, PyObject *);
static PyObject *PyX52_X52_setsecond(PyX52_X52_object *, PyObject *, PyObject *);
static PyObject *PyX52_X52_setdate(PyX52_X52_object *, PyObject *, PyObject *);

static PyObject *
PyX52_X52_new(PyTypeObject *type)
{

	PyX52_X52_object *self;
	self = (PyX52_X52_object *)type->tp_alloc(type, 0);
	if (self != NULL)
	{
		self->handle = NULL;
	}
	return (PyObject *)self;
}

static int
PyX52_X52_init(PyX52_X52_object *self)
{

	self->handle = x52_init();
	if (self->handle == NULL)
	{
		PyErr_SetString(PyExc_RuntimeError, "No compatible joysticks found.");
		return -1; 
	}
	x52_debug(self->handle, 0);
	return 0;
}

static void
PyX52_X52_dealloc(PyX52_X52_object *self)
{

	if (self->handle != NULL)
	{
		x52_close(self->handle);
	}
	self->handle = NULL;
	self->ob_type->tp_free((PyObject *)self);
}

static PyObject *
PyX52_X52_gettype(PyX52_X52_object *self)
{
	enum x52_type type = x52_gettype(self->handle);
	return Py_BuildValue("i", type);
}

static PyObject *
PyX52_X52_settext(PyX52_X52_object *self, PyObject *args, PyObject *kws)
{
	if (x52_gettype(self->handle) == DEV_YOKE)
	{
		PyErr_SetString(PyExc_RuntimeError, "Cannot call settext on this device.");
		return NULL;
	}

	int line_no = 0;
	PyObject *line = Py_BuildValue("s", "");

	static char *kwlist[] = {"line_no", "line", NULL};
	if (!PyArg_ParseTupleAndKeywords(args, kws, "iO", kwlist, &line_no, &line))
		return NULL;

	if (line_no < 0 || line_no > 2)
	{
		PyErr_SetString(PyExc_AttributeError, "line_no must be in the range 0-2.");
		return NULL;
	}

	if (!PyString_Check(line))
	{
		PyErr_SetString(PyExc_AttributeError, "line must be a string or subtype of string.");
		return NULL;
	}

	char *line_buf;
	if (PyString_Size(line) > 0)
	{
		line_buf = (char *)malloc(sizeof(char)*17);
		memset(line_buf, '\0', 16);
		strncpy(line_buf, PyString_AsString(line), 17);
	} else
	{
		line_buf = "";
	}

	if (x52_settext(self->handle, line_no, line_buf, strlen(line_buf)) != 0)
	{
		PyErr_SetString(PyExc_RuntimeError, "Could not set text.");
		return NULL;
	}

	Py_INCREF(Py_None);
	return Py_None;
}

static PyObject *
PyX52_X52_setbri(PyX52_X52_object *self, PyObject *args, PyObject *kws)
{
	if (x52_gettype(self->handle) == DEV_YOKE)
	{
		PyErr_SetString(PyExc_RuntimeError, "Cannot call setbri on this device.");
		return NULL;
	}

	int mode = 0;
	int bri = 0;

	static char *kwlist[] = {"mode", "bri", NULL};
	if (!PyArg_ParseTupleAndKeywords(args, kws, "ii", kwlist, &mode, &bri))
		return NULL;

	if (mode != 0 && mode != 1)
	{
		PyErr_SetString(PyExc_AttributeError, "Invalid mode.");
		return NULL;
	}

	if (mode == 0 && x52_gettype(self->handle) != DEV_X52PRO)
	{
		PyErr_SetString(PyExc_AttributeError, "Cannot set LED brightness on this device.");
		return NULL;
	}

	if (bri < 0 || bri > 127)
	{
		PyErr_SetString(PyExc_AttributeError, "Invalid brightness value.");
		return NULL;
	}

	if (x52_setbri(self->handle, mode, bri) != 0)
	{
		PyErr_SetString(PyExc_RuntimeError, "Could not set brightness.");
		return NULL;
	}	

	Py_INCREF(Py_None);
	return Py_None;
}

static PyObject *
PyX52_X52_setled(PyX52_X52_object *self, PyObject *args, PyObject *kws)
{
	if (x52_gettype(self->handle) != DEV_X52PRO)
	{
		PyErr_SetString(PyExc_RuntimeError, "Cannot call setled on this device.");
		return NULL;
	}

	/*
	 * TODO: Allow passing of a dict with LEDs/states
	 * TODO: Allow abstraction to button colour (i.e. PyX52.X52.buttons["coolie"].orange())
	 */
	int led = 0;
	int state = 0;

	static char *kwlist[] = {"led", "state", NULL};
	if (!PyArg_ParseTupleAndKeywords(args, kws, "ii", kwlist, &led, &state))
		return NULL;

	if (state != 0 && state != 1)
	{
		PyErr_SetString(PyExc_AttributeError, "Invalid LED state.");
		return NULL;
	}

	if (led < X52PRO_LED_FIRE || led > X52PRO_LED_IGREEN)
	{
		PyErr_SetString(PyExc_AttributeError, "Invalid LED ID.");
		return NULL;
	}

	if (x52_setled(self->handle, led, state) != 0)
	{
		PyErr_SetString(PyExc_RuntimeError, "Could not set LED status.");
		return NULL;
	}

	Py_INCREF(Py_None);
	return Py_None;
}

static PyObject *
PyX52_X52_settime(PyX52_X52_object *self, PyObject *args, PyObject *kws)
{
	int h24 = 1;
	
	struct tm *ltime = NULL;
	time_t t = time(NULL);
	ltime = gmtime(&t);

	int hour = ltime->tm_hour;
	int minute = ltime->tm_min;
	
	static char *kwlist[] = {"hour", "minute", "h24", NULL};
	if (!PyArg_ParseTupleAndKeywords(args, kws, "|iii", kwlist, &hour, &minute, &h24))
		return NULL;

	if (x52_settime(self->handle, h24, hour, minute) != 0)
	{
		PyErr_SetString(PyExc_RuntimeError, "Could not set time.");
		return NULL;
	}

	Py_INCREF(Py_None);
	return Py_None;
}

static PyObject *
PyX52_X52_setoffs(PyX52_X52_object *self, PyObject *args, PyObject *kws)
{
	/*
	 * TODO: default to local timezones
	 */
	if (x52_gettype(self->handle) == DEV_YOKE)
	{
		PyErr_SetString(PyExc_RuntimeError, "Cannot call setoffs on this device.");
		return NULL;
	}

	int offs2 = NULL;
	int offs3 = NULL;

	static char *kwlist[] = {"offs2", "offs3", NULL};
	if (!PyArg_ParseTupleAndKeywords(args, kws, "|ii", kwlist, &offs2, &offs3))
		return NULL;

	int inv2, inv3 = 0;
	if (offs2 != NULL)
	{
		if (offs2 < 0)
		{
			inv2 = 1;
			offs2 *= -1;
		}
		x52_setoffs(self->handle, 0, 1, inv2, offs2);
	}
	if (offs3 != NULL)
	{
		if (offs3 < 0)
		{
			inv3 = 1;
			offs3 *= -1;
		}
		x52_setoffs(self->handle, 1, 1, inv3, offs3);
	}

	Py_INCREF(Py_None);
	return Py_None;
}

static PyObject *
PyX52_X52_setsecond(PyX52_X52_object *self, PyObject *args, PyObject *kws)
{
	if (x52_gettype(self->handle) != DEV_YOKE)
	{
		PyErr_SetString(PyExc_RuntimeError, "Cannot call setsecond on this device.");
		return NULL;
	}

	struct tm *ltime;
	time_t t = time(NULL);
	gmtime_r(&t, ltime);

	int second = ltime->tm_sec;

	static char *kwlist[] = {"second", NULL};
	if (!PyArg_ParseTupleAndKeywords(args, kws, "|iii", kwlist, &second))
		return NULL;

	x52_setsecond(self->handle, second);

	Py_INCREF(Py_None);
	return Py_None;
}

static PyObject *
PyX52_X52_setdate(PyX52_X52_object *self, PyObject *args, PyObject *kws)
{
	if (x52_gettype(self->handle) == DEV_YOKE)
	{
		PyErr_SetString(PyExc_RuntimeError, "Cannot call setdate on this device.");
		return NULL;
	}

	struct tm *ltime;
	time_t *t;
	time(t);
	ltime = gmtime(t);

	int year = ltime->tm_year - 100;
	int month = ltime->tm_mon + 1;
	int day = ltime->tm_mday;

	static char *kwlist[] = {"year", "month", "day", NULL};
	if (!PyArg_ParseTupleAndKeywords(args, kws, "|iii", kwlist, &year, &month, &day))
		return NULL;

	if (x52_setdate(self->handle, year, month, day) != 0)
	{
		PyErr_SetString(PyExc_RuntimeError, "Could not set date.");
		return NULL;
	}

	Py_INCREF(Py_None);
	return Py_None;
}

static PyMethodDef PyX52_X52_methods[] = {
	{"gettype", (PyCFunction)PyX52_X52_gettype, METH_NOARGS, "Get the type of X52 device."},
	{"settext", (PyCFunction)PyX52_X52_settext, METH_VARARGS | METH_KEYWORDS, "Set the text on the MFD. (X52 and X52 Pro only.)"},
	{"setbri", (PyCFunction)PyX52_X52_setbri, METH_VARARGS | METH_KEYWORDS, "Set the brightness of either the LEDs or the MFD. (X52 and X52 Pro only.)"},
	{"setled", (PyCFunction)PyX52_X52_setled, METH_VARARGS | METH_KEYWORDS, "Set an LED's state. (X52 Pro only.)"},
	{"settime", (PyCFunction)PyX52_X52_settime, METH_VARARGS | METH_KEYWORDS, "Set the MFD display time. If arguments are not supplied, sets the current time in 24 hour format. (X52, X52 Pro and Yoke.)"},
	{"setoffs", (PyCFunction)PyX52_X52_setoffs, METH_VARARGS | METH_KEYWORDS, "Set the time offset. (X52, X52 Pro.)"},
	{"setsecond", (PyCFunction)PyX52_X52_setsecond, METH_VARARGS | METH_KEYWORDS, "Set the second. If no arguments are supplied, current second is used. (Yoke only.)"},
	{"setdate", (PyCFunction)PyX52_X52_setdate, METH_VARARGS | METH_KEYWORDS, "Set the date. If no arguments are supplied, current date is used. (X52, X52 Pro.)"},
	{NULL, NULL, 0, NULL}
};

static PyTypeObject PyX52_X52_type = {
        PyObject_HEAD_INIT(NULL)
        0,                         /*ob_size*/
        "PyX52.X52",                 /*tp_name*/
        sizeof(PyX52_X52_object),  /*tp_basicsize*/
        0,                         /*tp_itemsize*/
        (destructor)PyX52_X52_dealloc, /*tp_dealloc*/
        0,                         /*tp_print*/
        0,                         /*tp_getattr*/
        0,                         /*tp_setattr*/
        0,                         /*tp_compare*/
        0,                         /*tp_repr*/
        0,                         /*tp_as_number*/
        0,                         /*tp_as_sequence*/
        0,                         /*tp_as_mapping*/
        0,                         /*tp_hash */
        0,                         /*tp_call*/
        0,                         /*tp_str*/
        0,                         /*tp_getattro*/
        0,                         /*tp_setattro*/
        0,                         /*tp_as_buffer*/
        Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
        "X52 joystick object",     /* tp_doc */
        0,                         /*tp_traverse*/
        0,                         /* tp_clear */
        0,                         /* tp_richcompare */
        0,                         /* tp_weaklistoffset */
        0,                         /* tp_iter */
        0,                         /* tp_iternext */
        PyX52_X52_methods,         /* tp_methods */
        0,                         /* tp_members */
        0,                         /* tp_getset */
        0,                         /* tp_base */
        0,                         /* tp_dict */
        0,                         /* tp_descr_get */
        0,                         /* tp_descr_set */
        0,                         /* tp_dictoffset */
        (initproc)PyX52_X52_init,  /* tp_init */
        0,                         /* tp_alloc */
        (newfunc)PyX52_X52_new,    /* tp_new */
};

static PyMethodDef PyX52_methods[] = {
	{NULL}
};

#ifndef PyMODINIT_FUNC
#define PyMODINIT_FUNC void
#endif
PyMODINIT_FUNC
initPyX52(void)
{
	PyObject *m;

	if (PyType_Ready(&PyX52_X52_type) < 0)
		return;

	m = Py_InitModule3("PyX52", PyX52_methods, "Provides functions for handling Saitek X52, X52 Pro and Yoke joystick devices.");
	if (m == NULL)
		return;
	
	Py_INCREF(&PyX52_X52_type);
	PyModule_AddObject(m, "X52", (PyObject *) &PyX52_X52_type);

	PyModule_AddIntConstant(m, "LED_FIRE", X52PRO_LED_FIRE);
	PyModule_AddIntConstant(m, "LED_ARED", X52PRO_LED_ARED);
	PyModule_AddIntConstant(m, "LED_AGREEN", X52PRO_LED_AGREEN);
	PyModule_AddIntConstant(m, "LED_BRED", X52PRO_LED_BRED);
	PyModule_AddIntConstant(m, "LED_BGREEN", X52PRO_LED_BGREEN);
	PyModule_AddIntConstant(m, "LED_DRED", X52PRO_LED_DRED);
	PyModule_AddIntConstant(m, "LED_DGREEN", X52PRO_LED_DGREEN);
	PyModule_AddIntConstant(m, "LED_ERED", X52PRO_LED_ERED);
	PyModule_AddIntConstant(m, "LED_EGREEN", X52PRO_LED_EGREEN);
	PyModule_AddIntConstant(m, "LED_T1RED", X52PRO_LED_T1RED);
	PyModule_AddIntConstant(m, "LED_T1GREEN", X52PRO_LED_T1GREEN);
	PyModule_AddIntConstant(m, "LED_T2RED", X52PRO_LED_T2RED);
	PyModule_AddIntConstant(m, "LED_T2GREEN", X52PRO_LED_T2GREEN);
	PyModule_AddIntConstant(m, "LED_T3RED", X52PRO_LED_T3RED);
	PyModule_AddIntConstant(m, "LED_T3GREEN", X52PRO_LED_T3GREEN);
	PyModule_AddIntConstant(m, "LED_CORED", X52PRO_LED_CORED);
	PyModule_AddIntConstant(m, "LED_COGREEN", X52PRO_LED_COGREEN);
	PyModule_AddIntConstant(m, "LED_IRED", X52PRO_LED_IRED);
	PyModule_AddIntConstant(m, "LED_IGREEN", X52PRO_LED_IGREEN);

	PyModule_AddIntConstant(m, "BUTTON_TRIGGER1", X52PRO_BUTTON_TRIGGER1);
	PyModule_AddIntConstant(m, "BUTTON_FIRE", X52PRO_BUTTON_FIRE);
	PyModule_AddIntConstant(m, "BUTTON_A", X52PRO_BUTTON_A);
	PyModule_AddIntConstant(m, "BUTTON_B", X52PRO_BUTTON_B);
	PyModule_AddIntConstant(m, "BUTTON_C", X52PRO_BUTTON_C);
	PyModule_AddIntConstant(m, "BUTTON_PINKIE", X52PRO_BUTTON_PINKIE);
	PyModule_AddIntConstant(m, "BUTTON_D", X52PRO_BUTTON_D);
	PyModule_AddIntConstant(m, "BUTTON_E", X52PRO_BUTTON_E);
	PyModule_AddIntConstant(m, "BUTTON_T1", X52PRO_BUTTON_T1);
	PyModule_AddIntConstant(m, "BUTTON_T2", X52PRO_BUTTON_T2);
	PyModule_AddIntConstant(m, "BUTTON_T3", X52PRO_BUTTON_T3);
	PyModule_AddIntConstant(m, "BUTTON_T4", X52PRO_BUTTON_T4);
	PyModule_AddIntConstant(m, "BUTTON_T5", X52PRO_BUTTON_T5);
	PyModule_AddIntConstant(m, "BUTTON_T6", X52PRO_BUTTON_T6);
	PyModule_AddIntConstant(m, "BUTTON_TRIGGER2", X52PRO_BUTTON_TRIGGER2);
	PyModule_AddIntConstant(m, "BUTTON_MOUSE_CLICK", X52PRO_BUTTON_MOUSE_CLICK);
	PyModule_AddIntConstant(m, "BUTTON_MOUSE_SCROLL_FORWARD", X52PRO_BUTTON_MOUSE_SCROLL_FORWARD);
	PyModule_AddIntConstant(m, "BUTTON_MOUSE_SCROLL_BACK", X52PRO_BUTTON_MOUSE_SCROLL_BACK);
	PyModule_AddIntConstant(m, "BUTTON_MOUSE_SCROLL_CLICK", X52PRO_BUTTON_MOUSE_SCROLL_CLICK);
	PyModule_AddIntConstant(m, "BUTTON_COOLIE_UP", X52PRO_BUTTON_COOLIE_UP);
	PyModule_AddIntConstant(m, "BUTTON_COOLIE_RIGHT", X52PRO_BUTTON_COOLIE_RIGHT);
	PyModule_AddIntConstant(m, "BUTTON_COOLIE_DOWN", X52PRO_BUTTON_COOLIE_DOWN);
	PyModule_AddIntConstant(m, "BUTTON_COOLIE_LEFT", X52PRO_BUTTON_COOLIE_LEFT);
	PyModule_AddIntConstant(m, "BUTTON_THROTTLEHAT_BACK", X52PRO_BUTTON_THROTTLEHAT_BACK);
	PyModule_AddIntConstant(m, "BUTTON_THROTTLEHAT_RIGHT", X52PRO_BUTTON_THROTTLEHAT_RIGHT);
	PyModule_AddIntConstant(m, "BUTTON_THROTTLEHAT_FORWARD", X52PRO_BUTTON_THROTTLEHAT_FORWARD);
	PyModule_AddIntConstant(m, "BUTTON_THROTTLEHAT_LEFT", X52PRO_BUTTON_THROTTLEHAT_LEFT);
	PyModule_AddIntConstant(m, "BUTTON_MODE1", X52PRO_BUTTON_MODE1);
	PyModule_AddIntConstant(m, "BUTTON_MODE2", X52PRO_BUTTON_MODE2);
	PyModule_AddIntConstant(m, "BUTTON_MODE3", X52PRO_BUTTON_MODE3);
	PyModule_AddIntConstant(m, "BUTTON_I", X52PRO_BUTTON_I);
	PyModule_AddIntConstant(m, "BUTTON_MFD_FUNCTION", X52PRO_BUTTON_MFD_FUNCTION);
	PyModule_AddIntConstant(m, "BUTTON_MFD_STARTSTOP", X52PRO_BUTTON_MFD_STARTSTOP);
	PyModule_AddIntConstant(m, "BUTTON_MFD_RESET", X52PRO_BUTTON_MFD_RESET);
	PyModule_AddIntConstant(m, "BUTTON_MFD_PAGE_UP", X52PRO_BUTTON_MFD_PAGE_UP);
	PyModule_AddIntConstant(m, "BUTTON_MFD_PAGE_DN", X52PRO_BUTTON_MFD_PAGE_DOWN);
	PyModule_AddIntConstant(m, "BUTTON_MFD_SEL_UP", X52PRO_BUTTON_MFD_SEL_UP);
	PyModule_AddIntConstant(m, "BUTTON_MFD_SEL_DOWN", X52PRO_BUTTON_MFD_SEL_DOWN);
	PyModule_AddIntConstant(m, "BUTTON_MFD_SEL", X52PRO_BUTTON_MFD_SEL);

	PyModule_AddIntConstant(m, "TYPE_X52", DEV_X52);
	PyModule_AddIntConstant(m, "TYPE_X52PRO", DEV_X52PRO);
	PyModule_AddIntConstant(m, "TYPE_YOKE", DEV_YOKE);
}
