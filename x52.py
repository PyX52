#!/usr/bin/env python
from os import system
from traceback import print_exc
import time

build = system("setup.py build")

x52 = None

try:
	print "Importing PyX52...             ",
	import PyX52
	print "[  OK  ]"

	print "Creating PyX52.X52...          ",
	x52 = PyX52.X52()
	print "[  OK  ]"

	x52types = {PyX52.TYPE_X52: "X52", PyX52.TYPE_X52PRO: "X52 Pro", PyX52.TYPE_YOKE: "Yoke"}
	print "Detecting X52 device type...   ",
	typestr = x52types[x52.gettype()]
	print "[  OK  ]"
	print "X52 device type:                [ %s ]" % typestr
	
	print "Setting date...                ",
	x52.setdate()
	print "[  OK  ]"

	print "Setting time...                ",
	x52.settime()
	print "[  OK  ]"

	print "Setting MFD brightness...      ",
	x52.setbri(0, 127)
	time.sleep(0.1)
	x52.setbri(0, 63)
	time.sleep(0.1)
	x52.setbri(0, 0)
	time.sleep(0.1)
	x52.setbri(0, 127)
	print "[  OK  ]"

	print "Setting LED brightness...      ",
	x52.setbri(1, 127)
	time.sleep(0.1)
	x52.setbri(1, 63)
	time.sleep(0.1)
	x52.setbri(1, 0)
	time.sleep(0.1)
	x52.setbri(1, 127)
	print "[  OK  ]"

	print "Setting text...                ",
	x52.settext(0, "1234567890123456")
	time.sleep(0.1)
	x52.settext(1, "1234567890123456")
	time.sleep(0.1)
	x52.settext(2, "1234567890123456")
	time.sleep(0.1)
	x52.settext(0, "")
	time.sleep(0.1)
	x52.settext(1, "")
	time.sleep(0.1)
	x52.settext(2, "")
	print "[  OK  ]"

	print "Setting LED states...          ",
	for i in range(PyX52.LED_FIRE, PyX52.LED_IGREEN+1):
		x52.setled(i, 1)
		time.sleep(0.1)
		x52.setled(i, 0)
	print "[  OK  ]"

	print "Setting time offset...         ",
	x52.setoffs(60, -5*60)
	print "[  OK  ]"
except:
	print "[ FAIL ]"
	print_exc()
	exit(-1)
