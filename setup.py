#!/usr/bin/env python
from distutils.core import setup, Extension
PyX52 = Extension(
		'PyX52',
		sources=['PyX52.c'],
		libraries=['x52pro'],
		)
setup(name='PyX52',
  version='0.1',
  ext_modules=[PyX52],
)
