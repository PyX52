X52 Pro Button Mappings

Trigger stage 1                 0
Fire                            1
A                               2
B                               3
C                               4
Pinkie switch                   5
D                               6
E                               7
T1                              8
T2                              9
T3                              10
T4                              11
T5                              12
T6                              13
Trigger stage 2                 14
Mouse click                     15
Mouse scroll forward            16
Mouse scroll backward           17
Mouse scroll click              18
Coolie up                       19
Coolie right                    20
Coolie down                     21
Coolie left                     22
Throttle hat back               23
Throttle hat right              24
Throttle hat forward            25
Throttle hat left               26
Mode 1                          27
Mode 2                          28
Mode 3                          29
i                               30
MFD Function                    31
MFD Start/Stop                  32
MFD Reset                       33
MFD Pg Up                       34
MFD Pg Down                     35
MFD Select Up                   36
MFD Select Down                 37
MFD Select                      38
